﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Emedia2
{
    public partial class Form1 : Form
    {
        public string sciezka { get; set; }

        public Form1()
        {
            InitializeComponent();
            init();
        }

        public void init()
        {
            this.WindowState = FormWindowState.Maximized;
        }

        public void Wczytaj_oryginal()
        {
            
            Box1.Image = new Bitmap(sciezka);
            Box1.Width = Box1.Image.Width / 5;
            Box1.Height = Box1.Image.Height / 5;
            Box1.SizeMode = PictureBoxSizeMode.StretchImage;
            
        }

        public void Rysuj_FFT()
        {
            //Box2.Image = new Bitmap(sciezka);
            var fft = new FFT();
            Box2.Image = fft.fft(sciezka);
            Box2.Width = Box2.Image.Width / 5;
            Box2.Height = Box2.Image.Height / 5;
            Box2.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        public void Wczytaj_sciezke()
        {
            OpenFileDialog dlg = new OpenFileDialog { FileName = "", Filter = "JPEG Images (*.jpg)|*.jpg" };
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                sciezka = dlg.FileName;
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Wczytaj_sciezke();
            Wczytaj_oryginal();
        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Rysuj_FFT();
        }
        
    }
}

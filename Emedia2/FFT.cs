﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using AForge.Imaging;

namespace Emedia2
{
    public class FFT
    {
        public Bitmap fft(string sciezka)
        {
            var btm = new Bitmap(sciezka);

            btm.Save(sciezka + "_2");

            btm = new Bitmap(sciezka + "_2");

            //return NaSzaro(btm);
            return Forge(btm);
        }

        private Bitmap NaSzaro(Bitmap c)
        {
            Bitmap d = c;
            int x, y;

            for (x = 0; x < c.Width; x++)
            {
                for (y = 0; y < c.Height; y++)
                {
                    Color pixelColor = c.GetPixel(x, y);
                    int grey = (int)(pixelColor.R * 0.3 + pixelColor.G * 0.59 + pixelColor.B * 0.11);
                    d.SetPixel(x, y, Color.FromArgb(pixelColor.A, grey, grey, grey));
                }
            }
            return d;
        }

        public static Bitmap Convert(Bitmap oldbmp)
        {
            using (var ms = new MemoryStream())
            {
                oldbmp.Save(ms, ImageFormat.Gif);
                ms.Position = 0;
                return (Bitmap)System.Drawing.Image.FromStream(ms);
            }
        }

        private Bitmap Forge(Bitmap mapa)
        {
            Bitmap grayScaleBP = Convert(NaSzaro(mapa));
            // create complex image
            ComplexImage complexImage = ComplexImage.FromBitmap(grayScaleBP); // must be greyscale
            // do forward Fourier transformation
            complexImage.ForwardFourierTransform();
            // get complex image as bitmat
            Bitmap fourierImage = complexImage.ToBitmap();

            return fourierImage; 
        }
    }
}
